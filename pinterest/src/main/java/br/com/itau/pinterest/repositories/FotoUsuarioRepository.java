
package br.com.itau.pinterest.repositories;

import org.springframework.data.repository.CrudRepository;
import br.com.itau.pinterest.models.FotoUsuario;

public interface FotoUsuarioRepository extends CrudRepository<FotoUsuario, String> {
}
