package br.com.itau.investimento;

public class Investimento {
	private int id;
	private String nome;
	private String mes;
	private double valor;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.nome = mes;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
}


