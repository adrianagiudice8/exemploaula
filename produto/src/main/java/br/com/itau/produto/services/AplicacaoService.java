package br.com.itau.produto.services;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.itau.produto.models.Aplicacao;
import br.com.itau.produto.models.Cliente;
import br.com.itau.produto.models.Produto;
import br.com.itau.produto.models.Transacao;
import br.com.itau.produto.repositories.AplicacaoRepository;
import br.com.itau.produto.repositories.TransacaoRepository;
import br.com.itau.produto.viewobjects.Investimento;

@Service
public class AplicacaoService {
	@Autowired
	AplicacaoRepository aplicacaoRepository;

	@Autowired
	TransacaoRepository transacaoRepository;

	@Autowired
	ClienteService clienteService;

	@Autowired
	ProdutoService produtoService;

	RestTemplate restTemplate = new RestTemplate();

	public Optional<Investimento> criar(Investimento investimento) {
		Cliente cliente = restTemplate.getForObject("http://localhost:8082/cliente/" + investimento.getCliente().getCpf(), Cliente.class);
		investimento.setCliente(cliente);

		Produto produto = restTemplate.getForObject("http://localhost:8081/produto/" + investimento.getProduto().getId(), Produto.class);
		investimento.setProduto(produto);
		
		
		//Optional<Produto> produtoOptional = produtoService.buscar(investimento.getProduto().getId());

		//i//f (!produtoOptional.isPresent()) {
		//	return Optional.empty();
		//}

		//investimento.setProduto(produtoOptional.get());

		Aplicacao aplicacao = salvarAplicacao(investimento);
		investimento.setAplicacao(aplicacao);

		Transacao transacao = salvarTransacao(investimento);
		investimento.setTransacao(transacao);

		return Optional.of(investimento);
	}

	private Aplicacao salvarAplicacao(Investimento investimento) {
		Aplicacao aplicacao = new Aplicacao();

		aplicacao.setCliente(investimento.getCliente());
		aplicacao.setProduto(investimento.getProduto());
		aplicacao.setSaldo(investimento.getTransacao().getValor());
		aplicacao.setDataCriacao(LocalDate.now());

		return aplicacaoRepository.save(aplicacao);
	}

	private Transacao salvarTransacao(Investimento investimento) {
		Transacao transacao = investimento.getTransacao();

		transacao.setId(UUID.randomUUID());
		transacao.setAplicacao(investimento.getAplicacao());
		transacao.setTimestamp(LocalDateTime.now());

		return transacaoRepository.save(transacao);
	}
}
